
## Authored scientific research papers

### **New approach for visualization of relationships between RR and JT intervals**

| Paper | Year | DOI |
|:-----:| ---- |:---:|
| [![Download PDF][paper-pdf]](2017_PLOSOne_Ziaukas.pdf) | 2017 | [10.1371/journal.pone.0174279](http://dx.doi.org/10.1371/journal.pone.0174279) |

* Journal: **PLOS One**
* Authors:
  * Pranas Ziaukas
  * Abdullah Alabdulgader
  * Alfonsas Vainoras
  * Zenonas Navickas
  * Minvydas Ragulskis
* Keywords:
  * Electrocardiography 
  * Eigenvalues 
  * Depolarization 
  * Computational techniques 
  * Exercise 
  * Optimization 
  * Cardiovascular physiology 
  * Heart

<details>
<summary>Click to show full abstract.</summary>

This paper presents the concept of perfect matrices of Lagrange differences which are used to analyze relationships between RR and JT intervals during the bicycle ergometry exercise. The concept of the perfect matrix of Lagrange differences, its parameters, the construction of the load function and the corresponding optimization problem, the introduction of internal and external smoothing, embedding of the scalar parameter time series into the phase plane—all these computational techniques allow visualization of complex dynamical processes taking place in the cardiovascular system during the load and the recovery processes. Detailed analysis is performed with one person’s RR and JT records only—but the presented techniques open new possibilities for novel interpretation of the dynamics of the cardiovascular system.

</details>

---

### **Fractal dimension and Wada measure revisited**

| Paper | Year | DOI |
|:-----:| ---- |:---:|
| [![Download PDF][paper-pdf]](2016_NonlinearDynamics_Ziaukas.pdf) | 2016 | [10.1007/s11071-016-3281-4](http://dx.doi.org/10.1007/s11071-016-3281-4) |

* Journal: **Nonlinear Dynamics**
* Authors:
  * Pranas Ziaukas
  * Minvydas Ragulskis
* Keywords:
  * Chaotic attractor
  * Fractal dimension
  * Wada property 

<details>
<summary>Click to show full abstract.</summary>

An extended Newton’s discrete dynamical system with a complex control parameter is investigated in this paper. A novel computational algorithm is introduced for the evaluation of Wada measure. A nontrivial relationship between the fractal dimension and the Wada measure is revealed in NDDS. It is demonstrated that the reduction of the fractal dimension of basin boundaries of coexisting attractors does not automatically imply a lower Wada measure of these boundaries. Computational experiments are used to illustrate what impact the complexity of the relationship between fractal dimension and Wada measure does have in practical applications.

</details>

---

### **Competitively coupled maps for hiding secret visual information**

| Paper | Year | DOI |
|:-----:| ---- |:---:|
| [![Download PDF][paper-pdf]](2016_PhysicaA_Ziaukas.pdf) | 2016 | [10.1016/j.physa.2015.09.044](http://dx.doi.org/10.1016/j.physa.2015.09.044) |

* Journal: **Physica A**
* Authors:
  * Martynas Vaidelys
  * Pranas Ziaukas
  * Minvydas Ragulskis
* Keywords:
  * Self-organizing pattern
  * Competitive coupling
  * Image hiding

<details>
<summary>Click to show full abstract.</summary>

A novel digital image hiding scheme based on competitively coupled maps is presented in this paper. Self-organizing patterns produced by an array of non-diffusively coupled nonlinear maps are exploited to conceal the secret. The secret image is represented in the form of a dot-skeleton representation and is embedded into a spatially homogeneous initial state far below the noise level. Self-organizing patterns leak the secret image at a predefined set of system parameters. Computational experiments are used to demonstrate the effectiveness and the security of the proposed image hiding scheme.

</details>

---

### **Image hiding scheme based on the atrial fibrillation model**

| Paper | Year | DOI |
|:-----:| ---- |:---:|
| [![Download PDF][paper-pdf]](2015_AppliedSciences_Ziaukas.pdf) | 2015 | [10.3390/app5041980](http://dx.doi.org/10.3390/app5041980) |

* Journal: **Applied Sciences**
* Authors:
  * Martynas Vaidelys
  * Jurate Ragulskiene
  * Pranas Ziaukas
  * Minvydas Ragulskis
* Keywords:
  * Pattern formation
  * Atrial fibrillation
  * Communication scheme

<details>
<summary>Click to show full abstract.</summary>

An image communication scheme based on the atrial fibrillation (AF) model is presented in this paper. Self-organizing patterns produced by the AF model are used to hide and transmit secret visual information. A secret image is encoded into the random matrix of initial cell excitation states in the form of a dot-skeleton representation. Self-organized patterns produced by such initial cell states ensure a secure and efficient transmission of secret visual images. Procedures for digital encoding and decoding of secret images, as well as the sensitivity of the communication scheme to the perturbation of the AF model’s parameters are discussed in the paper.

</details>

---

### **Communication scheme based on evolutionary spatial games**

| Paper | Year | DOI |
|:-----:| ---- |:---:|
| [![Download PDF][paper-pdf]](2014_PhysicaA_Ziaukas.pdf) | 2014 | [10.1016/j.physa.2014.02.027](http://dx.doi.org/10.1016/j.physa.2014.02.027) |

* Journal: **Physica A**
* Authors:
  * Pranas Ziaukas
  * Tautvydas Ragulskis
  * Minvydas Ragulskis
* Keywords:
  * Pattern formation
  * Steganography
  * Communication scheme
  * Spatial game

<details>
<summary>Click to show full abstract.</summary>

A visual communication scheme based on evolutionary spatial games is proposed in this paper. Self-organizing patterns induced by complex interactions between competing individuals are exploited for hiding and transmitting secret visual information. Properties of the proposed communication scheme are discussed in details. It is shown that the hiding capacity of the system (the minimum size of the detectable primitives and the minimum distance between two primitives) is sufficient for the effective transmission of digital dichotomous images. Also, it is demonstrated that the proposed communication scheme is resilient to time backwards, plain image attacks and is highly sensitive to perturbations of private and public keys. Several computational experiments are used to demonstrate the effectiveness of the proposed communication scheme.

</details>

---

[paper-pdf]: https://img.shields.io/badge/paper-pdf-green.svg
